window.addEventListener("load", async function() {
'use strict'

const log = console.log;

const time_override = document.querySelector('#time_override');

time_override.addEventListener('change', _ev => update_ui(time_override.value));

(function() {
    //log('kickoff_wall_clock')
    const element = document.querySelector('#clock')
    var last_text = ''

    update_clock()

    function update_clock() {
        setTimeout(_ => requestAnimationFrame(update_clock), 300)

        const d = new Date()

        var text = [d.getHours(), d.getMinutes(), d.getSeconds()]
                    .map(left_pad)
                    .join(':')

        // optimize hitting the dom
        if (text !== last_text) {
            element.innerHTML = text
            last_text = text
        }
    }
})()

window.TimeTable = {
    render_table: render_table,
    update_ui: update_ui,
}

const talks_string = await fetch('/api/db.json')
const talks = await talks_string.json()
const table_dom = document.querySelector("#timetable")

log(talks[3])
render_table()
update_ui()


function render_table() {
    var html = ''
    var i = 0
    talks.forEach(talk => {
        var special = ''
        if (talk.special !== '') {
            special = '<span class="special">' + talk.special + '</span>'
        }

        html += `
            <tr id="event-${i}" class="${talk.talk_type}">
                <th>${left_pad(talk.start_time.hour)}<span class="little">${left_pad(talk.start_time.minute)}</span></th>
                <td>
                    <div>${special} ${talk.title}</div>
                    <div class="speakers">by ${talk.speakers.join(', ')}</div>
                </td>
            </tr>
        `
        i += 1
    })

    table_dom.innerHTML = html
}

function update_ui(current_time) {
    if (time_override.value != '' && time_override.value.len === 5 && time_override.value[2] === ':') {
        log(['time_override', time_override.value])
        current_time = time_override
    }
    if (typeof current_time === 'undefined') {
        let d = new Date()
        current_time = [d.getHours(), d.getMinutes()]
                    .map(left_pad)
                    .join(':')
    }
    log(current_time)
    var display_next_n_events = 2;
    var i = 0;
    talks.forEach(event => {
        const dom = document.querySelector('#event-'+i);
        const start_time = left_pad(event.start_time.hour) + ':' + left_pad(event.start_time.minute);
        if (display_next_n_events < 0 || start_time < current_time) {
            dom.classList.add('hidden');
        } else {
            dom.classList.remove('hidden');
            display_next_n_events -= 1;
        }

        i += 1;
    });

    setTimeout(_ => requestAnimationFrame(_ => update_ui()), 60*1000)
}


function left_pad(text) {
    return ('00' + text).substr(-2)
}

})
