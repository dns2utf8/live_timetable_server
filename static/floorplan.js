window.addEventListener("load", function() {
'use strict'

var login = null;
var status = null;

var people = [];

// TODO display the mascots to the user for selection
const images = {
    'Attendee': 'mascots/Attendee.svg',
    'Speaker': 'mascots/Speaker.svg',
    'Staff': 'mascots/Staff.svg',
};

const log = console.log
    , abs = Math.abs
    , pow = Math.pow
    , PI = Math.PI
    , random = function(min, max) {
        min = min || 0;
        max = max || 1;
        const delta = max - min;
        return Math.random() * delta + min;
    }

let ws = undefined;
setup_ws();

function setup_ws() {
    // automatically enable WebSocket over TLS if possible
    ws = new WebSocket('ws'+(location.protocol.indexOf('https') === 0 ? 's' : '')+'://'+location.host+'/live_api/ws/');

    ws.onclose = _ => {
        // TODO add more than one retry here
        requestAnimationFrame(_ => {
            log('reconnecting ws in a second');
            setTimeout(setup_ws, 1024);
        });
    }

    ws.onmessage = msg => {
        console.log(msg.data);
        const data = JSON.parse(msg.data);
        if (data === 'Updated') {
            console.log('logged in');
            // TODO maybe don't hide the UI here?
            document.getElementById("form-div").style.display = 'none';
            build_ui();
        } else if (data['CompleteSet'] !== undefined) {
            console.log('CompleteSet', data['CompleteSet']);
            people = data['CompleteSet'];
            build_ui();
            update_ui();
        } else if (data['UpdateEntry'] !== undefined) {
            console.log('UpdateEntry', data['UpdateEntry']);
            const entry = data['UpdateEntry'];
            people[entry[0]] = [entry[1], entry[2]];
            update_ui();
        } else {
            console.error(["unimplemented message", data]);
        }
    };

    ws.onopen = _ => {
        log('Connected.');

        ws.send('"GetAll"');
    }

    window.FloorPlan = {
        ws: ws,
        sendLogin: sendLogin,
    }
}

function build_ui() {
    document.getElementById('play-stage').parentNode.style.display = 'block';
}

function update_ui() {
    for (var key in people) {
        if (people.hasOwnProperty(key)) {
            var d = document.getElementById('user-' + key);
            if (!d) {
                d = document.createElement('div');
                var img = document.createElement('img');
                img.src = images[people[key][1]];
                img.height = "20";
                img.width = "20";
                d.id = 'user-' + key;
                d.style.position = 'absolute';
                var line = document.createElement('br');
                var span = document.createElement('span');
                span.innerText = key;
                span.style.fontSize = '18px';
                span.style.textShadow = '2px 0 0 #000, -2px 0 0 #000, 0 2px 0 #000, 0 -2px 0 #000, 1px 1px #000, -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000'

                d.appendChild(img);
                d.appendChild(line);
                d.appendChild(span);
                document.getElementById("play-stage").parentNode.appendChild(d);
            }
            d.style.top = people[key][0]['y'];
            // TODO magic number to center the whole thing nearer to the click of the user
            d.style.left = people[key][0]['x'] - 18;
        }
    }
}

function sendLogin() {
    login = document.getElementById('login-input').value;
    status = document.getElementById('login-status').value;
    ws.send(JSON.stringify({"Put":[login,{"x": random(0, 420) |0,"y": random(0, 420) |0}, status]}));
};

document.getElementById('play-stage').addEventListener('click', function(event) {
    if (login === null || status === null) {
        return;
    }
    var rect = event.target.getBoundingClientRect();
    var x = event.clientX - rect.left;
    var y = event.clientY - rect.top;

    ws.send(JSON.stringify({"Put":[login,{"x": parseInt(x), "y": parseInt(y)}, status]}));
});

});
