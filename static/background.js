window.addEventListener("load", function() {
'use strict'

const log = console.log
    , abs = Math.abs
    , pow = Math.pow
    , PI = Math.PI
    , random = function(min, max) {
        min = min || 0;
        max = max || 1;
        const delta = max - min;
        return Math.random() * delta + min;
    }


var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );

var renderer = new THREE.WebGLRenderer( { alpha: true } );
renderer.setSize( window.innerWidth, window.innerHeight );
document.querySelector('#background').appendChild( renderer.domElement );


const mascots = ['Attendee', 'Rustfest_Rainbow_TShirt', 'Rustfest_Logo-03'].map(name => {

    var texture = new THREE.TextureLoader().load( "/mascots/"+name+".svg" );

    var material = new THREE.MeshBasicMaterial( {
        color: 0xffffff,
        map: texture,
    } );

    return material
})
var geometry = new THREE.BoxGeometry( 1, 1, 1 );


var cubes = [];
for (let i = 0; i < 12; ++i) {
    const material = mascots[i % mascots.length];

    let cube = new THREE.Mesh( geometry, material );

    cube.rotation.x = random(-PI, PI);
    cube.rotation.y = random(-PI, PI);

    cube.position.x = random(-5, 5);
    cube.position.y = random(-5, 5);
    cube.position.z = random(-3, 3);
    cubes.push(cube);

    scene.add( cube );
}

ensure_minimal_distances();

camera.position.z = 5;


animate();
onWindowResize();
window.addEventListener( 'resize', onWindowResize, false );



function animate() {
    requestAnimationFrame( animate );

    cubes.forEach(cube => {
        cube.rotation.x += random(0.001, 0.01);
        cube.rotation.y += random(0.001, 0.01);

        cube.position.x += 0.01;
        if (cube.position.x > 8) {
            // reset x
            cube.position.x *= -1;
            // radomize the rest
            cube.position.y = random(-5, 5);
            cube.position.z = random(-3, 3);
        }
    });

    ensure_minimal_distances();

    renderer.render( scene, camera );
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}

function ensure_minimal_distances() {
    for (let i = 0; i < cubes.length; ++i) {
        const source = cubes[i];
        for (let j = i + 1; j < cubes.length; ++j) {
            const candidate = cubes[j];

            let too_close = true;
            do {
                const d_x = diff(source.position.x, candidate.position.x);
                const d_y = diff(source.position.y, candidate.position.y);
                const d_z = diff(source.position.z, candidate.position.z);

                const delta2 = pow(d_x, 2) + pow(d_y, 2) + pow(d_z, 2);
                if (delta2 < 1.4*3) {
                    too_close = true;

                    candidate.position.y = random(-5, 5);
                    candidate.position.z = random(-3, 3);
                } else {
                    too_close = false;
                }
            } while (too_close);
        }
    }
}

function diff(a, b) {
    return abs(a - b);
}

})
