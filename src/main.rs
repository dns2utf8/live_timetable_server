extern crate actix;
extern crate actix_web;
extern crate walkdir;
extern crate yaml_rust as yaml;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
#[macro_use]extern crate lazy_static;

//use std::cell::Cell;
use std::cmp::Ordering;
use std::collections::HashMap;

use actix::*;
use actix_web::*;

const DB_DUMP_FILE: &str = "mascot_positions.json";

lazy_static! {
static ref DB: Vec<Event> = load_db();
}
fn main() {
    print_available_commands();

    let sys = actix::System::new("live_api");

    let addr = std::env::var("LISTEN_ADDR").unwrap_or("[::1]:8080".into());

    println!("loading talks db ...");
    DB.len();

    let db_addr = SyncArbiter::start(1, || {
        println!("trying to load last state from {:?}", DB_DUMP_FILE);
        let mut db = std::fs::read_to_string(DB_DUMP_FILE)
            .into_iter()
            .flat_map(|text: String| {
                serde_json::from_str(&text)
            });
        let positions = match db.next() {
            Some(positions) => {
                println!("  restored state from disk");
                positions
            }
            None => {
                println!("  error loading last state");
                HashMap::new()
            }
        };

        MascotPositionStorage {
            positions,
            subscribers: vec![],
        }
    });

    println!("binding to {:?} override LISTEN_ADDR to change", addr);
    server::new(move|| {
        vec![
            App::with_state(State { db: db_addr.clone() })
                .resource("/live_api/ws/", |r| {
                    r.f(|req| {
                        println!("new WebSocket");
                        ws::start(
                            &req.drop_state(),
                            WsClientConnection {
                                db: req.state().db.clone(),
                            },
                        )
                    })
                })
                .resource("/api/db.json", |r| r.f(move |_req| Json(&*DB)))
                .handler(
                    "/",
                    fs::StaticFiles::new("./static")
                        .expect("unable to locate ./static folder")
                        //.show_files_listing()
                        .index_file("index.html"),
                ),
        ]
    }).bind(addr)
    .unwrap()
    .start();

    let _ = sys.run();
}

fn load_db() -> Vec<Event> {
    let base_path = std::env::var("LIVE_DATA_DIR").unwrap_or("/home/vp/rustfest.eu/2019_Barcelona/barcelona.rustfest.eu".into());
    println!("loading talk data from {:?} - set LIVE_DATA_DIR to override", base_path);
    let mut v = Vec::new();

    let speakers = std::fs::read_to_string(format!("{}/_data/speakers.yml", base_path)).expect("unable to read speakers file");
    let speakers = yaml::YamlLoader::load_from_str(&speakers).expect("invalid yml file");
    let speakers = &speakers[0];

    let walker = walkdir::WalkDir::new(format!("{}/_sessions", base_path))
        .same_file_system(true)
        .into_iter()
        .filter_map(|e| e.ok())
        .filter(|e| {
            // println!("  () {:?}", e.path());
            e.file_type().is_file() &&
            e.file_name()
                .to_str()
                .map(|s| {
                    let (first, _rest) = s.split_at(1);

                    first != "_"// && rest.ends_with(".md")
                }).unwrap_or(false)
        })
        ;

    for entry in walker {
        let file_contents = std::fs::read_to_string(entry.path()).expect("unable to read file");
        let infos = yaml::YamlLoader::load_from_str(&file_contents).expect("invalid yml file");
        let info = &infos[0];

        if info["day"].as_str().map_or(false, |day| day != "saturday") {
            continue
        }
        if info["public"].as_bool().unwrap_or(false) == false {
            continue
        }


        let start_time = info["start"].as_str().expect("no start field").parse().expect("unable to parse start time");
        let talk_type = info["type"].as_str().unwrap();
        let title = info["title"].as_str().unwrap();
        let special = info["special"].as_str().unwrap_or("");
        let mut speakers = info["speakers"].as_vec().unwrap().iter()
            .filter_map(|speaker| {
                if let Some(short_name) = speaker.as_str() {
                    // resolve names
                    let speaker = &speakers[short_name]["name"];
                    // println!(" (speaker): {:?}", speaker);
                    return speaker.as_str();
                }
                None
            })
            .map(|s| s.into())
            .collect::<Vec<_>>();

        speakers.sort();


        // println!("  [] {:?}", info["public"].as_bool());
        v.push(Event {
            start_time,
            talk_type: talk_type.into(),
            title: title.into(),
            special: special.into(),
            speakers,
        });
    }

    v.push(Event {
        start_time: Time { hour: 9, minute: 30, },
        talk_type: "break".into(),
        title: "Opening<(Good_Morgning, Atendees, Sponsors, Staff)>".into(),
        special: "".into(),
        speakers: vec![],
    });

    v.push(Event {
        start_time: Time { hour: 18, minute: 45, },
        talk_type: "break".into(),
        title: "Closing Announcements".into(),
        special: "".into(),
        speakers: vec![],
    });

    /*v.push(Event {
        start_time: Time { hour: 18, minute: 45, },
        talk_type: "break".into(),
        title: "Family photo & Closing Venue".into(),
        special: "".into(),
        speakers: vec![],
    });*/

    v.sort();

    //println!("Events: {:#?}", v);

    v
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Clone, Serialize)]
struct Event {
    start_time: Time,
    talk_type: String,
    title: String,
    special: String,
    speakers: Vec<String>,
}

impl std::cmp::Ord for Event {
    fn cmp(&self, other: &Event) -> Ordering {
        self.start_time.cmp(&other.start_time)
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Clone, Serialize)]
struct Time {
    hour: u8,
    minute: u8,
}
impl std::cmp::Ord for Time {
    fn cmp(&self, other: &Time) -> Ordering {
        match self.hour.cmp(&other.hour) {
            Ordering::Equal => self.minute.cmp(&other.minute),
            r => r,
        }
    }
}
impl std::str::FromStr for Time {
    type Err = std::num::ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.split(":");

        Ok(Time {
            hour: parts.next().expect("Time::parse").parse()?,
            minute: parts.next().expect("Time::parse").parse()?,
        })
    }
}


#[derive(Debug, Clone, Message, Serialize, Deserialize)]
pub struct Position {
    x: f64,
    y: f64,
}

struct State {
    db: Addr<MascotPositionStorage>,
}
impl Actor for State {
    type Context = SyncContext<Self>;
}

#[derive(Default)]
pub struct MascotPositionStorage {
    positions: HashMap<String, (Position, String)>,
    subscribers: Vec<Addr<WsClientConnection>>,
}

impl Actor for MascotPositionStorage {
    type Context = SyncContext<Self>;
}

impl MascotPositionStorage {
    fn broadcast(&mut self, name: String, pos: Position, mascot: String) {
        let message = PositionProtocolAnswer::UpdateEntry(name, pos, mascot);

        self.subscribers.iter().for_each(|subscriber| {
           subscriber.do_send(message.clone());
        });
    }
}

impl Handler<PositionProtocol> for MascotPositionStorage {
    type Result = MessageResult<PositionProtocol>;

    fn handle(&mut self, incoming: PositionProtocol, _ctx: &mut Self::Context) -> Self::Result {
        use PositionProtocol::*;
        use PositionProtocolAnswer::*;
        println!("incoming message {:?}", incoming);
        let r = match incoming {
            GetAll => CompleteSet(self.positions.clone()),
            Put(name, pos, mascot) => {
                // notify others
                self.broadcast(name.clone(), pos.clone(), mascot.clone());

                self.positions.insert(name, (pos, mascot));

                // persist
                use std::io::Write;
                let db = serde_json::to_string(&self.positions).expect("unable to serialize internal state");

                std::fs::File::create(DB_DUMP_FILE)
                    .into_iter()
                    .filter_map(|mut permanent_storage| {
                        permanent_storage.write_all(db.as_bytes()).ok()
                    })
                    .map(|_:()| {
                        println!("persisted state do disk");
                        Persisted
                    })
                    .next()
                    .unwrap_or(PersistedVolatile)
            }
            Delete(name) => {
                self.positions.remove(&name);
                Deleted
            }
        };
        MessageResult(r)
    }
}
impl Handler<Subscribe> for MascotPositionStorage {
    type Result = ();

    fn handle(&mut self, incoming: Subscribe, _ctx: &mut Self::Context) -> Self::Result {
        println!("incoming Subscribe");

        self.subscribers.push(incoming.0);
    }
}
impl Handler<Unsubscribe> for MascotPositionStorage {
    type Result = ();

    fn handle(&mut self, incoming: Unsubscribe, _ctx: &mut Self::Context) -> Self::Result {
        println!("incoming Subscribe");

        self.subscribers.retain(|e| *e != incoming.0);
    }
}

/// Internal Subscription System for alerting other clients
/// Subscribe to process signals.
#[derive(Message)]
struct Subscribe(pub Addr<WsClientConnection>);
#[derive(Message)]
struct Unsubscribe(pub Addr<WsClientConnection>);


/// Define http actor
struct WsClientConnection {
    db: Addr<MascotPositionStorage>,
}

impl Actor for WsClientConnection {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        println!("hurray I live");
        self.db.do_send(Subscribe(ctx.address()));
    }

    fn stopped(&mut self, ctx: &mut Self::Context) {
        self.db.do_send(Unsubscribe(ctx.address()));
    }
}

/// Actually distribute the broadcast messages to the clients
impl Handler<PositionProtocolAnswer> for WsClientConnection {
    type Result = MessageResult<PositionProtocolAnswer>;

    fn handle(&mut self, msg: PositionProtocolAnswer, ctx: &mut Self::Context) -> Self::Result {
        use PositionProtocolAnswer::*;
        if let UpdateEntry(name, pos, mascot) = msg {
            ctx.text(serde_json::to_string(&UpdateEntry(name, pos, mascot)).expect("unable to serialize internal state"));

            MessageResult(())
        } else {
            unreachable!("must not receive other variant in impl Handler<PositionProtocolAnswer> for MascotPositionStorage");
        }
    }
}

/// Handler for ws::Message message
impl StreamHandler<ws::Message, ws::ProtocolError> for WsClientConnection {
    fn handle(&mut self, msg: ws::Message, ctx: &mut Self::Context) {
        match msg {
            ws::Message::Ping(msg) => ctx.pong(&msg),
            ws::Message::Text(text) => {
                let message: Result<PositionProtocol, _> = serde_json::from_str(&text);

                if let Ok(message) = message {
                    //self.db.send(ListAll)
                    self.db.send(message)
                    .into_actor(self)
                    .then(|res, _, ctx| {
                        if let Ok(others) = res {
                            ctx.text(serde_json::to_string(&others).expect("unable to serialize internal state"));
                        }
                        fut::ok( () )
                    })
                    .spawn(ctx);
                } else {
                    ctx.text(serde_json::to_string(&PositionProtocolAnswer::Error(format!("Invalid message: {:?}", text))).expect("unable to serialize internal state"))
                }
            }
            ws::Message::Binary(bin) => ctx.binary(bin),
            _ => (),
        }
    }
}

#[derive(Debug, Clone, Message, Serialize, Deserialize)]
#[rtype(result = "PositionProtocolAnswer")]
pub enum PositionProtocol {
    GetAll,
    Put(String, Position, String),
    Delete(String),
}

#[derive(Debug, Clone, Message, Serialize, Deserialize)]
pub enum PositionProtocolAnswer {
    CompleteSet(HashMap<String, (Position, String)>),
    UpdateEntry(String, Position, String),
    Persisted,
    PersistedVolatile,
    Deleted,
    Error(String),
}

fn print_available_commands() {
    use PositionProtocol::*;
    use PositionProtocolAnswer::*;
    println!("Available commands over WebSocket:");

    println!("    {:?} => {}", GetAll, serde_json::to_string(&GetAll).unwrap());

    let put = Put("bla".into(), Position{ x: 42.0, y: 42.0, }, "Speaker".into());
    println!("    {:?} => {}", put, serde_json::to_string(&put).unwrap());

    let update_entry = UpdateEntry("bla".into(), Position{ x: 42.0, y: 42.0, }, "Speaker".into());
    println!("    {:?} => {}", update_entry, serde_json::to_string(&update_entry).unwrap());

    let delete = Delete("bla".into());
    println!("    {:?} => {}", delete, serde_json::to_string(&delete).unwrap());

    println!("");
}
