# Setup local development environement

Clone the code repos:
```
git clone https://gitlab.com/dns2utf8/live_timetable_server.git
git clone https://github.com/RustFestEU/rome.rustfest.eu.git
```

Update to latest stable and install cargo-watch:
```
rustup update stable
cargo install cargo-watch
```

Change directory and start a local service:
```
cd live_timetable_server
LIVE_DATA_DIR=../rome.rustfest.eu RUST_BACKTRACE=1 cargo watch -x run
```

Connect with you browser at [localhost:8080](http://localhost:8080) per default.

If that does not work because your port is occupied or there is an IPv6 problem you can override the listen address like this for eg. IPv4 on port 8082:
```
LIVE_DATA_DIR=../rome.rustfest.eu LISTEN_ADDR=127.0.0.1:8082 RUST_BACKTRACE=1 cargo watch -x run
```

![Screenshot from RustFest Rome](InfoBeamer_during_Keynote.png)

# Goals

- [x] Deliver schedule for infobeamer at RustFest Rome
- [x] Allow the crowd to play a mini multiplayer game
- [ ] Imlement events for updating changes on other clients

# Architecture

- Easy import data from yaml files
- Actix.rs Webserver
- WebSocket on the same Port
    - Enables multiplayer games

# Updates

Reads the data from a cloned rome.rustfest.eu repo.

# Deployment

Instance at https://rustfest.estada.ch

```
certbot certonly --webroot --agree-tos -w /srv/letsencrypt_confirmation/ -d rustfest.estada.ch
```

Listens on `[::1]:8080` by default

Manually started inside a screen.
