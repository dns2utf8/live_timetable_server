CARGO_TARGET_DIR=

deploy: pack actual_deploy
	# deploy done

actual_deploy:
	rsync -av --delete rustfest.estada.ch@schatten.estada.ch:project/mascot_positions.json target/deploy/
	rsync -av --delete target/deploy/ rustfest.estada.ch@schatten.estada.ch:project

clean:
	rm -rf target/deploy
	mkdir -p target/deploy

release:
	cargo build --release
	cp target/release/live_timetable_server target/deploy/
debug:
	cargo build
	cp target/debug/live_timetable_server target/deploy/

pack: clean release actual_pack
	#done

actual_pack:
	rsync -a static target/deploy/
	rsync -a ~/rustfest.eu/2019_Barcelona/barcelona.rustfest.eu/_data target/deploy/
	rsync -a ~/rustfest.eu/2019_Barcelona/barcelona.rustfest.eu/_sessions target/deploy/
